---
title: "Mingw64 Crosscompile C/C++ on Linux for Windows - Part I"
date: 2019-01-14T17:20:17+01:00
draft: false
---
## Background
As much as I love the simplicity of building software for the host
system's architecture, there comes a time when you have to provide
binaries for another system all together. It seems the simplest solution
is to just get that other system running, either as a virtual machine
or on a spare system, copy the source code over and use the system's
native toolchains to recompile the target. Often this is not possible
however, as either the system does not provide any toolchain to do
so, or the system's architecture makes it much more annoying to do the
actual compiling. For instance, you wouldn't want to compile Firefox
on a Raspberry Pi (arm-cortex), as it takes way longer than just using
pre-built binaries from someone that crosscompiled them from a faster
system. I recently hit another barrier when I was tasked to build
an application for the Windows operating system. As the application
was mostly written for my bachelor thesis, when I had the freedom
of choosing my weapons, it is aimed at a Linux build environment.
Therefore, the libraries and frameworks were chosen based on the Linux
system's confinement. Here is a dependency listing for my application:

* Qt5 (Graphical User Interface)
* OpenCV (For processing images)
* Tesseract (Optical Character Recognition)
* Sqlite3 (Data Persistance)

For now, let's stick with that list. If we want to build this
application on Windows, we first need to install all these dependencies.
So, the Windows struggle begins. As a Linux regular, I'm used to the
power and simplicity of package managers. Even on Mac, it seems
that any serious developer does not bother with installing software
libraries any other way. I thought to just start with a simple sample
Qt5 application from one of their tutorials. Even though I got the
application to work, it left me with strained nerves. At least the
Qt team put together a more or less working installer. In contrast,
the OpenCV library needs to be compiled from source, if you want to
use their developer headers. So, either you go the Microsoft way and
use their Visual Studio Compiler or the GNU way and use the Cygwin
compiler. I naively chose the first option: following the OpenCV [doc]
(https://docs.opencv.org/4.0.1/d3/d52/tutorial_windows_install.html)
page for installing CV on Windows, I got the OpenCV libraries working on
my machine. Two down, two more to go. Even though I greatly disliked the
nonuniform way of installing, I started to feel positive that I would
soon have my software merrily running. Well... I guess I wouldn't write
this if that was the case. My attempt to install the Tesseract library
could be summarised as a miserable failure. The Github page shows two
methods of installing Tesseract. The first one is an outdated installer
by the University of Mannheim. The second involves compiling from source
- once again. In order to achieve the latter, one needs to use a sort of
CMake wrapper called CPPAN, which resolves and downloads dependencies. I
didn't like the sound of that method but tried it nonetheless. To some
degree I was able to get results. I ran into a weird situation, however.
Somehow the root paths for the header-files seemed to be different in
the CPPAN installed version. I ended up using guards to hack my way
around this peculiarity.
``` C++
#ifdef _WIN32
#include <baseapi.h>
#include <allheaders.h>
#else
    #include <tesseract/baseapi.h>
    #include <leptonica/allheaders.h>
#endif
```
This is where I thought, why the hell am I going through this? Isn't
every dependency that is going to be added later bound to be another
trial-and-error-solution-finding-session? Also, I've spent way too
much time on this alien operating system. I just wished for a simple
solution like a package manager that I could use to install all those
dependencies. Well as it turns out, the best way forward is to leave
package managing to those who know how to do it. The Linux community.

## The Mingw-W64 Project
On Fedora's Mingw description it says:

> The Fedora MinGW project's mission is to provide an excellent
> development environment for Fedora users who wish to cross-compile
> their programs to run on Windows, minimizing the need to use Windows
> at all.

Well, that sounds more like it! The best thing is we leave all the
dependency management to the package manager. So, installation is as easy
as it can get.

Apt:

```
# apt get install mingw-w64
```

Yum:

```
# yum install mingw-w64
```

Pacman based:

As the mingw-w64 does not reside in the official repositories, we
will need to get them from the AUR (Arch User Repository). You will
then download the desired PKGBUILD and build the application using
`makepkg`. While this procedure is very easy, there are also a bunch
of AUR-helpers. I am currently using `yay`. For installation see their
github [page](https://github.com/Jguer/yay). After installation you
can use `yay` in the same way that you would use `pacman`.

```
yay -S mingw-w64-gcc
```

As I only went through this whole process on Arch Linux, for the rest of
this article I will assume you will find the right commands for your own
distribution. Arch Linux makes a lot of things a lot easier, as there
are a ton of precompiled packages that the AUR Community put together
and maintains. Anyway let us just quickly check if our base toolchain works.

``` C++
#include <iostream>

int main(int argc, char *argv[])
{
    std::cout << "Hello Windows World" << std::endl;
    return 0;
}
```

Compile:
```
x86_64-w64-mingw32-g++ -static main.cpp -o test
```

If you don't get any error message, everything should work fine. But
let's try things out, so we can be sure. To make it easier, we can
install Wine, which is a compatability layer capable of running Windows
applications on a Unix operating system.

```
# pacman -S wine
```

After that you will be able to run our test program through the Wine
emulation layer:

```
wine test.exe
> Hello Windows World
```

## Cross compiling, the nice way
Ok, so our test application is running. But an application that only
depends on the standard library is hardly worth the effort of setting
up a cross compilation environment. In most real life applications,
we have many more external dependencies. Additionally, the build of
the application is most likely also more elaborate (Copying files,
downloading assests, setting up configuration and so on). One could
say an application is as much defined by the build tool, as it is by
the actual application code. Many people use Makefiles that define the
build process and build order. As it is very tedious to write these
Makefiles by hand, espescially if the build process should be supported
on different systems, build managers like CMake have been created to
simplify the process. There is a CMake wrapper that sets a number of
parameters, so we can use it to generate Makefiles that use the Mingw
crosscompiler.

```
yay -S mingw-w64-cmake
```

A CMake Project is set up by writing a CMakeLists.txt file, in which we
define the various build setting. So, we could set up a test project the
following way:

``` Cmake
# Set Project Name
project(HelloWindowsTest) 

# Set the minimum version of cmake required to runh
cmake_minimum_required(VERSION 3.1) 

# Set compiler to use c++14 standard
SET(CMAKE_CXX_STANDARD 14)

# Use static libraries instead of dynamic
SET(STATIC_FLAGS "-static")
SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${STATIC_FLAGS}")

add_executable(test
    main.cpp
)
```

Save the file and build the project:

```
mkdir build && cd build
x86_64-w64-mingw32-cmake ..
make
```
You will hopefully find a `test.exe` file in the build folder that can
be testet again by starting it with Wine.

So, this is it for the first part of the Mingw cross compilation
tutorial. In the next article we will look at how to install mingw
libraries and how to integrate them into our application using CMake. On
the way we will learn how to create our own `PKGBUILD` scripts, that can
be used to install software on Arch Linux. Also, we will look at how to
ship software that was built using Mingw.
