---
title: "Mingw64 Crosscompile C/C++ on Linux for Windows - Part II"
date: 2019-01-28T11:26:32+01:00
draft: false
---

## Recap
In the last post, we set up a basic cross compiling environment. We used
the Mingw compiler to compile a simple "Hello World" application, which
we than started with Wine. At the end, we prepared a CMake project for
the same appliction. If you want to read up on any of those steps,
you can read them up [here]({{< ref "mingw64-crosscompile.md" >}}).

## The Plan
As much as we can (and should) congratulate ourselves for our
achievements, we are still a long way from compiling an actual useful
application. That is to say, if you are content with writing to the
screen and getting some input from the user, you can already far from
here. But if you or your user want to see some shiny user interface, you
will probably incorperate some kind of user interface. For that, you
might even have your favourite framework in mind. Since this article
deals with C++ applications that need to be run on a Windows machine,
the most obvious candidate for such a framework will be Qt. Getting our
application to show some Qt window and maybe some widget will therefore
be our first priority. Also, it is just so much more fun to see an actual
user interface pop up, instead of writing some commands in the console
and getting boring output back. If you read the first article, you
will maybe remember that my motivation for this whole ordeal stems
from a specific application, which had dependencies to QT, OpenCV,
Tesseract, which in turn were frustrating to install on
Windows. Thus, the listed dependencies will need to be installed and
somehow incorperated into the application as well.

## Installing Qt5
The Qt5 installation is really the easiest part.

```
yay -S mingw-w64-qt5-base
```

Now let us just write a small Qt test application. For now, we are
satisfied with an application window that contains a button.

``` C++
#include <QApplication>
#include <QPushButton>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QPushButton button ("Hello World!");
    button.show();
    return app.exec();
}
```

In the previous article we used CMake to organise our project. CMake
also makes it easy to find libraries on a build system and link to them.
For this we use the CMake command `find_package`.

```
project(QtTest)
cmake_minimum_required(VERSION 3.1)

# Set compiler to use c++14 standard
SET(CMAKE_CXX_STANDARD 14)

SET(STATIC_FLAGS "-static-libgcc -static-libstdc++ -Bstatic -lpthread -Wl,-Bdynamic")
SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${STATIC_FLAGS}")

# look for Qt5 on build system
find_package(Qt5 COMPONENTS Core Gui Widgets REQUIRED)
  
add_executable(test
    main.cpp
    )

# link to the libraries found by find_package
target_link_libraries(test 
    Qt5::Core Qt5::Gui Qt5::Widgets
    )

```

As before create a build directory and run `x86_64-w64-mingw32-cmake ..`
to generate the Makefile. Afterwards build the application with `make`.
Start the `test.exe` with Wine. Aaaand...

```
wine test.exe 
002a:err:module:import_dll Library Qt5Core.dll (which is needed by L"Z:\\home\\moritzmg\\Code\\qt_test\\build\\test.exe") not found
002a:err:module:import_dll Library Qt5Widgets.dll (which is needed by L"Z:\\home\\moritzmg\\Code\\qt_test\\build\\test.exe") not found
002a:err:module:attach_dlls Importing dlls for L"Z:\\home\\moritzmg\\Code\\qt_test\\build\\test.exe" failed, status c0000135
```

Damn! That would have been too easy, wouldn't it. In order to fix the
missing dll error, we need to locate the dlls and then copy them into
the executable's folder. For now let's do this manually. The dlls reside
in `/usr/x86_64-w64-mingw32/bin`.

```
cp /usr/x86_64-w64-mingw32/bin/Qt5Core.dll . # <-- In build folder
cp /usr/x86_64-w64-mingw32/bin/Qt5Widgets.dll .
cp /usr/x86_64-w64-mingw32/bin/Qt5Core.dll . 
```

Well if you start the application now, there will be even more missing
dll errors. The reason for this is that the Qt libraries depend on
yet other shared libraries. This also means that we can get rid of
the static declaration in our `CMakeLists.txt`, because we are not
compiling statically anyway. Unfortunately, without a more elaborate way
of handling the dlls you will have to manually copy all the dlls in the
executable's folder - no way around it. Just find the right one and copy
it over. Repeat this until no more errors occur. At the end of the process you
should be greeted by a Qt window that contains this "beautiful" button.

![Image](images/helloworld-qtwine.png)

# On to the next: OpenCV
Now I don't know about you but seeing this really makes me happy.
Anyway let's get a bit more involved now. Next on the list of
want-to-have's is the OpenCV dependency. If you like me use Arch Linux
and `yay` you can just issue `yay -Ss mingw opencv` which in turn will
let you know which package to install. So let us follow the 
recommendation.

```
yay -S mingw-w64-opencv
```

Ok great. This should install the right package. So to make life easy
let us write another simple test application. As a test application we just
copy the code from the OpenCV tutorial and see if we get it to compile and
start.

``` C++
#include <opencv2/opencv.hpp>

using namespace cv;

int main( int argc, char** argv )
{
	Mat image;
	// Make sure you have a test.png in binary folder or rename
	image = imread("test.png", IMREAD_COLOR);
	Mat gray_image;
	cvtColor(image, gray_image, COLOR_BGR2GRAY);
	imwrite("Gray_Image.jpg", gray_image );
	namedWindow(imageName, WINDOW_AUTOSIZE);
	namedWindow("Gray image", WINDOW_AUTOSIZE);
	imshow(imageName, image);
	imshow("Gray image", gray_image);
	waitKey(0);
	return 0;
}

```

So we set up a new CMake project. I had trouble using the
`find_package` command for `OpenCV`. Instead I used a tool called
`pkg-config`. This tool must be installed first:

```
yay -S mingw-w64-pkg-config
```

CMake can use `pkg-config` to find the right library paths and set the
right compilation flags. This time we will link statically in order to
avoid copying all those dlls. The full `CMakeLists.txt` looks like this:

```
project(OpenCVTest)
cmake_minimum_required(VERSION 3.11)

set(CMAKE_CXX_STANDARD 14)

SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -static")
find_package(PkgConfig REQUIRED)
pkg_check_modules(OpenCV REQUIRED opencv4)

add_executable(
    prog
    main.cpp
)

target_link_libraries(
    prog
    ${OpenCV_LIBRARIES}
)

target_include_directories(
    prog
    PRIVATE
    ${OpenCV_INCLUDE_DIRS}
)

file(
    COPY ${CMAKE_SOURCE_DIR}/test.png
    DESTINATION ${CMAKE_BINARY_DIR}
)
```

As you can see we also added a file command, that copies a test image
to the binary directory. Make sure you place an image into the source
directory and you are good to run cmake.

```
x86_64-w64-mingw32-cmake .. # <- In build folder
make
```

Try running the program. Hopefully you will some color to grayscale
conversion happening.


# Tesseract
With OpenCV out of the way, we turn to the Tesseract library, which can
be used for optical character recognition. This time we will have to put
in a bit more work to optain the library, because at the time of writing
there is no package in the Arch User Repository. But don't worry, it is
actually pretty simple to write our own. For this we use the Arch Linux
build system and the `makepkg` command. The `makepkg` command reads a
`PKGBUILD` file that defines all the nescessary build steps. From the
Arch Linux wiki we can copy a stub and fill out the blanks. In the end
the `PKGBUILD` should look like this:

```
pkgname=mingw-w64-tesseract
_pkgname=tesseract
pkgver=4.0.0
pkgrel=1
pkgdesc="An OCR Engine that was developed at HP Labs between 1985 and 1995 (mingw-w64)"
arch=('any')
url="https://github.com/tesseract-ocr/tesseract"
license=('Apache')
depends=(
    'mingw-w64-gcc'
    'mingw-w64-libtiff'
    'mingw-w64-libpng'
    'mingw-w64-leptonica'
    'mingw-w64-giflib'
    )
makedepends=(
    'git'
    'mingw-w64-gcc'
    'mingw-w64-configure'
    'mingw-w64-pkg-config'
    )
options=('!strip' '!buildflags' 'staticlibs')
source=("${url}/archive/${pkgver}.tar.gz")
md5sums=('SKIP')

_architectures="x86_64-w64-mingw32 i686-w64-mingw32"

build() {
    cd "$srcdir/${_pkgname}-${pkgver}"
    ./autogen.sh
    for _arch in ${_architectures}; do
        mkdir -p build-${_arch} && pushd build-${_arch}
        ${_arch}-configure ..
        make
        popd
    done
}

package() {
    for _arch in ${_architectures}; do
        cd "${srcdir}/${_pkgname}-${pkgver}/build-${_arch}"
        make DESTDIR="${pkgdir}" install
        ${_arch}-strip --strip-unneeded "${pkgdir}"/usr/${_arch}/bin/*.dll
        ${_arch}-strip -g "${pkgdir}"/usr/${_arch}/lib/*.a
    done
}
```

Before you can build this package though, you will need to install all the
dependencies that I listed in `depends`. If you go ahead and do that you might
find out that there is no such package as `mingw-w64-leptonica`. So you must go
ahead and write another `PKGBUILD` for that. It should look very
similar:

```
pkgname=mingw-w64-leptonica
_pkgname=leptonica
pkgver=1.77.0
pkgrel=1
pkgdesc="Leptonica Library (mingw-w64)"
arch=('any')
url="http://www.leptonica.org"
license=('BSD')
# groups=()
makedepends=("mingw-w64-gcc" "mingw-w64-pkg-config" "mingw-w64-configure")
depends=(
        mingw-w64-giflib
        mingw-w64-libtiff
        mingw-w64-libpng
        mingw-w64-libwebp
        mingw-w64-openjpeg2
        mingw-w64-zlib
        )
options=('!strip' '!buildflags' 'staticlibs')

source=(${url}/source/${_pkgname}-${pkgver}.tar.gz)
sha256sums=('dd7990ab6b6824b0cfed70920824d37b47184240f98db4085f7dbf1250cd4899')

_architectures="x86_64-w64-mingw32 i686-w64-mingw32"

build() {
	cd "${srcdir}/${_pkgname}-${pkgver}"
    for _arch in ${_architectures}; do
        mkdir -p build-${_arch} && pushd build-${_arch}
        ${_arch}-configure ..
        make
        popd
    done
}
package() {
  for _arch in ${_architectures}; do
      cd "${srcdir}/${_pkgname}-${pkgver}/build-${_arch}"
      make DESTDIR="${pkgdir}" install
      ${_arch}-strip --strip-unneeded "${pkgdir}"/usr/${_arch}/bin/*.dll
      ${_arch}-strip -g "${pkgdir}"/usr/${_arch}/lib/*.a
  done
}

```
It is best to place each of the `PKGBUILD`-files into a seperate
directory so you keepo track what's what.
Now we run makepkg to build and install our packages:

```
cd mingw-w64-leptonica && makepkg -si ; cd ..
cd mingw-w64-tesseract && makepkg -si ; cd ..
```

Alright, now we should have installed tesseract and leptonica to
`/usr/x86_64-w64-mingw32` and are ready to use them. For this we are going
to create another test project.

main.cpp:

```
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>

int main()
{
    char *outText;

    tesseract::TessBaseAPI *api = new tesseract::TessBaseAPI();
    // Initialize tesseract-ocr with English, without specifying tessdata path
    if (api->Init(NULL, "eng")) {
        fprintf(stderr, "Could not initialize tesseract.\n");
        exit(1);
    }

    // Open input image with leptonica library
    Pix *image = pixRead("phototest.tif");
    api->SetImage(image);
    // Get OCR result
    outText = api->GetUTF8Text();
    printf("OCR output:\n%s", outText);

    // Destroy used object and release memory
    api->End();
    delete [] outText;
    pixDestroy(&image);

    return 0;
}

```

CMakeLists.txt:
```
project(TesseractTest)
cmake_minimum_required(VERSION 3.11)

set(CMAKE_CXX_STANDARD 14)
find_package(PkgConfig REQUIRED)
pkg_check_modules(Tesseract REQUIRED tesseract lept)

add_executable(
    prog
    main.cpp
)

target_link_libraries(
    prog
    ${Tesseract_LIBRARIES}
)

target_include_directories(
    prog
    PRIVATE
    ${Tesseract_INCLUDE_DIRS}
)


file(
    COPY ${CMAKE_SOURCE_DIR}/tessdata/eng.traineddata
    DESTINATION ${CMAKE_BINARY_DIR}/tessdata
)

file(
    COPY ${CMAKE_SOURCE_DIR}/phototest.tif
    DESTINATION ${CMAKE_BINARY_DIR}
)
```

We will need a TIF image to test if tesseract is working properly, as well as a
pretrained model. You can either scan your own image or like me, take the image
from the tesseract test repository
([phototest.tif](https://github.com/tesseract-ocr/test/blob/master/testing/phototest.tif)).
Likewise, you will find a suitable model [here](https://github.com/tesseract-ocr/tessdata).
For some reason I could not get this program to link statically and I had to
copy all the missing dlls into the build folder again. 

If the program builds and executes we should see some output.

```
wine prog.exe                                                                               [14:04:34]
OCR output:
This is a lot of 12 point text to test the
ocr code and see if it works on all types
of file format.

The quick brown dog jumped over the
lazy fox. The quick brown dog jumped
over the lazy fox. The quick brown dog
jumped over the lazy fox. The quick
brown dog jumped over the lazy fox.
```
