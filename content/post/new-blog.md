---
title: "New Blog"
date: 2018-09-24T17:51:07+02:00
draft: false
---

This blog will be a collection of my experiences mostly relating to computer stuff. 
I will mainly keep notes in an effort to not make the same mistakes over and over again.
If it happens to help anyone else... GREAT! If you have comments or suggestions, don't 
hesitate to get in touch.
